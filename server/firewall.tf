/*

  Open a SSH port to the Google Identity Aware Proxy 

  To use this use the command:  gclod beta compute ssh %INSTANCENAME% --tunnel-through-iap

*/
resource "google_compute_firewall" "forseti-server-ssh-fw-ingress" {
  name    = "fw-ingress-forseti-server-ssh"
  network = "${var.vpc_network}"
  project = "${var.project_id}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = [
    "35.235.240.0/20",
  ]

  target_tags = ["fw-ssh-ingress"]
}
