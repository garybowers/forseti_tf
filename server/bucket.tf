/*

The Forseti server requires two storage buckets, one is the main
server bucket which contains the rules, configs and outputs.

The cai bucket is used to store the inventory outputs.

*/

resource "google_storage_bucket" "forseti-server-bucket" {
  project       = "${var.project_id}"
  name          = "${var.prefix}-server"
  location      = "${var.bucket_location}"
  force_destroy = true
  storage_class = "MULTI_REGIONAL"

  versioning {
    enabled = true
  }
}

resource "google_storage_bucket" "forseti-server-cai" {
  project       = "${var.project_id}"
  name          = "${var.prefix}-cai-export"
  location      = "${var.bucket_location}"
  force_destroy = true
  storage_class = "MULTI_REGIONAL"

  versioning {
    enabled = true
  }
}
