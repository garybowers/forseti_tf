resource "google_compute_health_check" "forseti-server-healthcheck" {
  project            = "${var.project_id}"
  name               = "${var.prefix}-server-autohealing-hc"
  check_interval_sec = 10
  timeout_sec        = 20
  healthy_threshold  = 2

  tcp_health_check {
    port = "50051"
  }
}

resource "google_compute_instance_group_manager" "forseti-server-igm" {
  project = "${var.project_id}"
  name    = "${var.prefix}-server-igm"
  zone    = "${var.zone}"

  base_instance_name = "forseti-server"
  update_strategy    = "REPLACE"

  target_size = 1

  instance_template = "${google_compute_instance_template.forseti-server-template.self_link}"
}

resource "google_compute_instance_template" "forseti-server-template" {
  name_prefix = "${var.prefix}-template-"
  description = "Forseti server template"

  machine_type = "n1-standard-1"
  project      = "${var.project_id}"

  lifecycle {
    create_before_destroy = true
  }

  tags = [
    "forseti-server",
    "forseti",
    "fw-ssh-ingress",
  ]

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    auto_delete  = true
    boot         = true
    source_image = "ubuntu-os-cloud/ubuntu-1804-lts"
    disk_type    = "pd-ssd"
    disk_size_gb = "60"
  }

  network_interface {
    subnetwork = "projects/${var.project_id}/regions/${var.region}/subnetworks/${var.subnetwork}"
  }

  service_account {
    email  = "${google_service_account.forseti-server.email}"
    scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }

  metadata_startup_script = <<SCRIPT
    #Install forseti
    #!/bin/bash
    exec > /var/log/forseti-deploy.log
    exec 2>&1
    sudo apt-get update -y
    sudo apt-get --assume-yes install google-cloud-sdk
    USER_HOME=/home/ubuntu
    
    #Install fluentd
    FLUENTD=$(ls /usr/sbin/google-fluentd)
    if [ -z "$FLUENTD" ]; then
      cd $USER_HOME
      curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
      bash install-logging-agent.sh
      sysctl enable google-fluentd
    fi

    #Install google cloud sql proxy
    CLOUD_SQL_PROXY=$(which cloud_sql_proxy)
    if [ -z "$CLOUD_SQL_PROXY" ]; then
       cd $USER_HOME
       wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
       sudo mv cloud_sql_proxy /usr/local/bin/cloud_sql_proxy
       chmod +x /usr/local/bin/cloud_sql_proxy
    fi  
  
    #Install dependencies
    sudo apt-get install -y git unzip mysql-client 

    #Aquire forseti
    cd $USER_HOME
    rm -rf *forseti*
    git clone https://github.com/GoogleCloudPlatform/forseti-security.git
    cd forseti-security
    git fetch --tags
    git fetch --all
    git checkout ${var.forseti_version}
   
    #Install dependencies
    sudo apt-get install -y $(cat install/dependencies/apt_packages.txt | grep -v "#" | xargs)
    pip install --upgrade pip==9.0.3
    pip install -q --upgrade setuptools wheel
    pip install -q --upgrade -r requirements.txt
   
    #Setup forseti logging
    touch /var/log/forseti.log
    chown ubuntu:root /var/log/forseti.log
    cp $USER_HOME/forseti-security/configs/logging/fluentd/forseti.conf /etc/google-fluentd/config.d/forseti.conf
    cp $USER_HOME/forseti-security/configs/logging/logrotate/forseti /etc/logrotate.d/forseti
    chmod 644 /etc/logrotate.d/forseti
    service google-fluentd restart
    logrotate /etc/logrotate.conf
    
    #Export variables needed by the installer
    export SQL_PORT=3306
    export SQL_INSTANCE_CONN_STRING="${var.project_id}:${var.region}:${google_sql_database_instance.forseti-server-db.name}"
    export FORSETI_DB_NAME="forseti_security"
    export FORSETI_HOME=$USER_HOME/forseti-security
    export FORSETI_SERVER_CONF=$FORSETI_HOME/configs/forseti_conf_server.yaml
    export SCANNER_BUCKET="${var.prefix}-server"
   
    #Set permissions on the folders
    chmod -R ug+rw $FORSETI_HOME/configs $FORSETI_HOME/rules $FORSETI_HOME/install/gcp/scripts/run_forseti.sh

    #Install forseti
    python setup.py install
   
    #Download server configuration from GCS
    gsutil -m cp gs://${var.prefix}-server/configs/forseti_conf_server.yaml $FORSETI_SERVER_CONF
    gsutil -m cp gs://${var.prefix}-server/rules/* $FORSETI_HOME/rules

    #Export the environment vars
    FORSETI_ENV=$(cat <<EOF
#!/bin/bash
export PATH=$PATH:/usr/local/bin
export FORSETI_HOME=/home/ubuntu/forseti-security
export FORSETI_SERVER_CONF=$FORSETI_HOME/configs/forseti_conf_server.yaml
export SCANNER_BUCKET="${var.prefix}-server}"     

EOF
)"
    echo "$FORSETI_ENV" > $USER_HOME/forseti_env.sh
    
    #Initialise the services
    bash $FORSETI_HOME/install/gcp/scripts/initialize_forseti_services.sh

    #Start services
    systemctl enable cloudsqlproxy
    systemctl start cloudsqlproxy
    sleep 5
    
    #Update the db schema
    python $FORSETI_HOME/install/gcp/upgrade_tools/db_migrator.py
    

    #Start the forseti services
    systemctl enable forseti
    systemctl start forseti
    
    USER=ubuntu
    
    (echo "5 */2 * * * (/usr/bin/flock -n /home/ubuntu/forseti-security/forseti_cron_runner.lock $FORSETI_HOME/install/gcp/scripts/run_forseti.sh || echo '[forseti-security] Warning: New Forseti cron job will not be started, because previous Forseti job is still running.') 2>&1 | logger") | crontab -u $USER -
    echo "Added the run_forseti.sh to crontab under user $USER"


    echo "Execution of startup script finished"

  SCRIPT
}
