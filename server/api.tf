/*

The Forseti Project in GCP needs the various API's turned on in order to access
those services across your organisation

*/

resource "google_project_service" "adminsdk" {
  project            = "${var.project_id}"
  service            = "admin.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "cloudsql-admin" {
  project            = "${var.project_id}"
  service            = "sqladmin.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "cloudsql" {
  project            = "${var.project_id}"
  service            = "sql-component.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "bigquery" {
  project            = "${var.project_id}"
  service            = "bigquery-json.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "replicapool" {
  project            = "${var.project_id}"
  service            = "replicapool.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "appengineadmin" {
  project            = "${var.project_id}"
  service            = "appengine.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "cloudassetapi" {
  project            = "${var.project_id}"
  service            = "cloudasset.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "deploymentmanager" {
  project            = "${var.project_id}"
  service            = "deploymentmanger.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "iam" {
  project            = "${var.project_id}"
  service            = "iam.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "iamcreds" {
  project            = "${var.project_id}"
  service            = "iamcredentials.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "storageapi" {
  project            = "${var.project_id}"
  service            = "storage-api.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "container" {
  project            = "${var.project_id}"
  service            = "container.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "servicemanagement" {
  project            = "${var.project_id}"
  service            = "servicemanagement.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "logging" {
  project            = "${var.project_id}"
  service            = "logging.googleapis.com"
  disable_on_destroy = false
}
