/*

We do not use the default compute service accounts, instead we will create one
for the server to use.  This service account will be relatively privileged.

*/

resource "google_service_account" "forseti-server" {
  account_id   = "svc-forseti-server"
  display_name = "Forseti Server"
  project      = "${var.project_id}"
}
