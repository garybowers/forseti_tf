variable "project_id" {
  type        = "string"
  description = "The project id forseti should be deployed to"
}

variable "prefix" {
  type        = "string"
  description = "The prefix for the name of all resources"
}

variable "bucket_location" {
  type        = "string"
  description = "The region for the storage buckets, defaults to EU"
}

variable "region" {
  type        = "string"
  description = "The region the services should exist, defaults to europe-west1"
}

variable "zone" {
  type        = "string"
  description = "The availability zone the resources should be created, defaults to europe-west1-d"
}

variable "vpc_network" {
  type        = "string"
  description = "The name of the vpc network to use, defaults to default"
}

variable "subnetwork" {
  type        = "string"
  description = "The name of the subnetwork that the compute instances should use"
}

variable "forseti_version" {
  type        = "string"
  description = "The version of forseti to install, defaults to v2.10.0"
}
