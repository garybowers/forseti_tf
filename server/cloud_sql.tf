resource "random_integer" "db-salt" {
  min = 100000
  max = 999999
}

resource "google_sql_database_instance" "forseti-server-db" {
  project = "${var.project_id}"
  name    = "${var.prefix}-forseti-db-${random_integer.db-salt.result}"

  database_version = "MYSQL_5_7"
  region           = "${var.region}"

  settings {
    tier = "db-n1-standard-1"

    disk_type = "PD_SSD"
    disk_size = "25"

    backup_configuration {
      binary_log_enabled = true
      enabled            = true
    }
  }
}

resource "google_sql_database" "forseti-server-database" {
  project   = "${var.project_id}"
  name      = "forseti_security"
  instance  = "${google_sql_database_instance.forseti-server-db.name}"
  charset   = "utf8"
  collation = "utf8_general_ci"
}

resource "google_sql_user" "forseti-server-sql-user" {
  project  = "${var.project_id}"
  name     = "root"
  instance = "${google_sql_database_instance.forseti-server-db.name}"
  host     = "%"
  password = ""
}
