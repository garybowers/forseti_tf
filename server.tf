module "forseti-server" {
  source     = "./server"
  prefix     = "${var.prefix}"
  project_id = "${var.project_id}"

  region = "${var.region}"
  zone   = "${var.zone}"

  vpc_network     = "${var.vpc_network}"
  subnetwork      = "${var.subnetwork}"
  bucket_location = "${var.bucket_location}"

  forseti_version = "${var.forseti_version}"
}
